package com.techpoa.examples;

import com.techpoa.beans.Person;
import com.techpoa.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GettingStarted {

    @Test
    public void imperativeApproach() throws IOException {
        // 1. Find people aged less or equal 18
        // 2. Then change implementation to find first 10 people
        List<Person> people = MockData.getPeople();
        List<Person> children = new ArrayList<>();

        for (Person person : people) {
            if (person.getAge() <= 18) {
                children.add(person);

                if (children.size() == 10) {
                    break;
                }
            }
        }

        children.forEach(System.out::println);
    }

    @Test
    public void declarativeApproachUsingStreams() throws Exception {
        List<Person> people = MockData.getPeople();
        List<Person> children = people
                .stream()
                .filter(p -> p.getAge() <= 18)
                .limit(10)
                .toList();

        children.forEach(System.out::println);

    }
}
