package com.techpoa.examples;

import com.techpoa.beans.Car;
import com.techpoa.beans.Person;
import com.techpoa.beans.PersonDTO;
import com.techpoa.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class TransformationsMapAndReduce {

    /**
     * Map function allows transformation from one datatype to another
     * We transform the data from Person to PersonDTO
     *
     * @throws IOException
     */
    @Test
    void yourFirstTransformationWithMap() throws IOException {
        List<Person> people = MockData.getPeople();

        List<PersonDTO> peopleDto = people.stream().map(p -> {
                    return new PersonDTO(p.getId(), p.getFirstName(), p.getAge());
                }).limit(10)
                .toList();

        System.out.println("tTransformationWithMap");
        peopleDto.forEach(System.out::println);

    }

    @Test
    void mapToDoubleAndFindAverageCarPrice() throws IOException {
        List<Car> cars = MockData.getCars();
    }

    @Test
    public void reduce() {
        int[] integers = {1, 2, 3, 4, 99, 100, 121, 1302, 199};
    }
}

